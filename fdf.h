/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fdf.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: atafah <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/01 15:04:20 by atafah            #+#    #+#             */
/*   Updated: 2019/03/05 22:08:26 by atafah           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <mlx.h>
#include "gnl/get_next_line.h" 
#include <fcntl.h>


typedef struct	s_point
{
	int x;
	int y;
}				t_point;


typedef struct	s_line
{
	char	**values;
	t_point	**points;

	struct	s_line *next;
}				t_line;


typedef struct	s_map
{
	t_line	*line;
	int		dimensions;
}				t_map;



t_map	*read_file(char *argv);
int		key_press(int key, void *param);
void	set_points(t_map *map);	
void	line(void *mlx, void *win, int x0, int y0, int x1, int y1, int color);
void	draw_points(t_map *map, void *ptr, void *win);
