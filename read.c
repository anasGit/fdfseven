/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   read.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: atafah <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/01 15:32:10 by atafah            #+#    #+#             */
/*   Updated: 2019/03/05 22:36:23 by atafah           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

t_map	*read_file(char *argv)
{
	int		fd;
	char	**output;
	t_line	*line;
	t_line	*head;
	t_map	*map;
	int		i;

	fd = open(argv, O_RDONLY);
	output = (char **)malloc(sizeof(char *));
	line = (t_line *)malloc(sizeof(t_line));

	if (get_next_line(fd, output))
	{
		i = 0;
		head = line;
		line->next = (t_line *)malloc(sizeof(t_line *));
	
		map = (t_map *)malloc(sizeof(t_map));
		line->next = (t_line *)malloc(sizeof(t_line *));
		
		map->line = head;
		map->line->next = line;
		
		map->line->values = ft_strsplit(*output, ' ');
	
		
		while (head->values[i])
			i++;
		map->dimensions = i;
	}

	while (get_next_line(fd, output))
	{
		line->values = ft_strsplit(*output, ' ');
		line->next = (t_line *)malloc(sizeof(t_line));
		line = line->next;
	}
	
	return (map);
}
