/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: atafah <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/01 15:17:02 by atafah            #+#    #+#             */
/*   Updated: 2019/03/05 22:24:59 by atafah           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

int main(int argc, char **argv)
{
	if (argc != 2)
	{
		ft_putstr("Usage : ./fdf file.fdf\n");
		return (0);
	}

	t_map	*map;
	void	*ptr;
	void	*win;
	if ((map = read_file(argv[1])))
	{	
		set_points(map);
		ptr = mlx_init();

		win = mlx_new_window(ptr, map->dimensions * 70, map->dimensions * 50, "FDF");
		mlx_string_put(ptr, win, 10, 10, 0xFFFFFF, "ESC : EXIT");

		draw_points(map, ptr, win);

		mlx_hook(win, 2, 0, &key_press, (void *)0);
		mlx_loop(ptr);
	}
	return (0);
}
