/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   points.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: atafah <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/05 20:17:36 by atafah            #+#    #+#             */
/*   Updated: 2019/03/05 22:38:26 by atafah           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

void	set_points(t_map *map)
{
	t_line	*line;
	int		i;
	int		j;
	int		spacex;
	int		spacey;

	line = map->line;
	spacey = 50;
	while (line)
	{
		i = 0;
		j = 0;
		spacex = 20;
		line->points = (t_point **)malloc(sizeof(t_point*) * map->dimensions);
		while (j <  map->dimensions)
		{
			line->points[i] = (t_point *)malloc(sizeof(t_point));
			line->points[i]->x = spacex;
			line->points[i]->y = spacey;
			i++;
			j++;
			spacex += 20;
		}
		line = line->next;
		spacey += 20;
	}
}


void	draw_points(t_map *map, void *ptr, void *win)
{
	int	i;

//	while (map->line)
//	{

			printf("%s\n", map->line->values[0]);
		i = 0;
		while (map->line->points[i + 1])
		{
			line (ptr, win,  map->line->points[i]->x, map->line->points[i]->y - ft_atoi(map->line->values[i]), map->line->points[i + 1]->x, map->line->points[i + 1]->y - ft_atoi(map->line->values[i + 1]),  0xFFFFFF);
			i++;
		}
		map->line = map->line->next;
//	}
}
